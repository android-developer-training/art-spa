package com.example.artspace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ArtSpaceScreen()
                }
            }
        }
    }
}

@Composable
fun ArtSpaceScreen(modifier: Modifier = Modifier) {
    val firstArtwork = R.drawable.casa_batllo
    val secondArtwork = R.drawable.casa_calvet
    val thirdArtwork = R.drawable.casa_mila
    val fourthArtwork = R.drawable.casa_vicens

    var title by remember {
        mutableStateOf(R.string.casa_batllo)
    }

    var year by remember {
        mutableStateOf(R.string.casa_batllo_year)
    }

    var currentArtwork by remember {
        mutableStateOf(firstArtwork)
    }

    Column(
        modifier = modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {


        when (currentArtwork) {
            firstArtwork -> {
                ArtworkDisplayAndTitle(
                    currentArtwork = currentArtwork,
                    title = title,
                    year = year,
                    onPreviousClick = {
                        currentArtwork = fourthArtwork
                        title = R.string.casa_vicens
                        year = R.string.casa_vicens_year
                    },
                    onNextClick = {
                        currentArtwork = secondArtwork
                        title = R.string.casa_calvet
                        year = R.string.casa_calvet_year
                    },
                )
            }

            secondArtwork -> {
                ArtworkDisplayAndTitle(
                    currentArtwork = currentArtwork,
                    title = title,
                    year = year,
                    onPreviousClick = {
                        currentArtwork = firstArtwork
                        title = R.string.casa_batllo
                        year = R.string.casa_batllo_year
                    },
                    onNextClick = {
                        currentArtwork = thirdArtwork
                        title = R.string.casa_mila
                        year = R.string.casa_mila_year
                    },
                )
            }

            thirdArtwork -> {
                ArtworkDisplayAndTitle(
                    currentArtwork = currentArtwork,
                    title = title,
                    year = year,
                    onPreviousClick = {
                        currentArtwork = secondArtwork
                        title = R.string.casa_calvet
                        year = R.string.casa_calvet_year
                    },
                    onNextClick = {
                        currentArtwork = fourthArtwork
                        title = R.string.casa_vicens
                        year = R.string.casa_vicens_year
                    },
                )
            }

            fourthArtwork -> {
                ArtworkDisplayAndTitle(
                    currentArtwork = currentArtwork,
                    title = title,
                    year = year,
                    onPreviousClick = {
                        currentArtwork = thirdArtwork
                        title = R.string.casa_mila
                        year = R.string.casa_mila_year
                    },
                    onNextClick = {
                        currentArtwork = firstArtwork
                        title = R.string.casa_batllo
                        year = R.string.casa_batllo_year
                    },
                )
            }
        }
    }
}

@Composable
fun ArtworkDisplayAndTitle(
    modifier: Modifier = Modifier,
    @DrawableRes currentArtwork: Int,
    @StringRes title: Int,
    @StringRes year: Int,
    onPreviousClick: () -> Unit,
    onNextClick: () -> Unit
) {
    Image(
        painter = painterResource(currentArtwork),
        contentDescription = stringResource(id = title),
        modifier = modifier.fillMaxWidth(),
        contentScale = ContentScale.FillWidth,

    )
    Spacer(modifier = modifier.size(16.dp))
    Column (
        modifier = Modifier.verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        // Artwork title
        Text(
            text = stringResource(id = title),
            fontWeight = FontWeight.Bold,
            color = colorResource(id = R.color.blue_100),
            fontSize = 32.sp
        )

        // Artwork year
        Text(
            text = "— ${stringResource(id = year)} —",
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium,
            color = colorResource(id = R.color.gray_300)
        )
    }
    Row(
        modifier = modifier.padding(horizontal = 8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterHorizontally)
    ){
        Button(
            onClick = onPreviousClick,
            colors = ButtonDefaults.buttonColors(
                containerColor = colorResource(id = R.color.gray_900)
            ),
            elevation = ButtonDefaults.buttonElevation(
                defaultElevation = 1.dp,
                pressedElevation = 0.dp,
                focusedElevation = 0.dp
            )
        ) {
            Text(
                text = "Previous",
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                color = colorResource(id = R.color.blue_300)
            )
        }
        Button(
            onClick = onNextClick,
            colors = ButtonDefaults.buttonColors(
                containerColor = colorResource(id = R.color.gray_900)
            ),
            elevation = ButtonDefaults.buttonElevation(
                defaultElevation = 1.dp,
                pressedElevation = 0.dp,
                focusedElevation = 0.dp
            )
        ) {
            Text(
                text = "Next",
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                color = colorResource(id = R.color.blue_300)
            )
        }

    }
}

@Preview(showBackground = true)
@Composable
fun ArtSpacePreview() {
    ArtSpaceTheme {
        ArtSpaceScreen()
    }
}